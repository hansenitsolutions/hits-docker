#!/bin/bash

docker run -d --name="sickrage" -v /home/sickrage:/config -v /mnt/Volume_1/download-tosort:/downloads -v /mnt/tv:/tv -e PGID=1 -e PUID=1 -p 8081:8081 linuxserver/sickrage:latest
