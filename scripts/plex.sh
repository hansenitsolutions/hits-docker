#!/bin/bash
iptables -P INPUT ACCEPT
docker image pull linuxserver/plex:latest
docker stop plex
docker rm plex

docker run \
    -d \
    --name=plex \
    --net=host \
    -e VERSION=latest \
    -e TZ="Australia/Melbourne" \
    -v /opt/plex/config:/config \
    -v /opt/movies:/data/movies \
    -v /opt/tv:/data/tv \
    -v /mnt/downloads:/data/downloads \
    -v /opt/plex/transcode:/transcode \
    --restart unless-stopped \
    linuxserver/plex
