#!/bin/bash
docker image pull linuxserver/sonarr:latest
docker stop sonarr
docker rm sonarr
docker run \
    --name sonarr \
    -p 8989:8989 \
    -e PUID=1 -e PGID=1 \
    -v /etc/localtime:/etc/localtime:ro \
    -v /opt/sonarr:/config \
    -v /mnt/media/tv:/tv \
    -v /mnt/downloads:/downloads \
    --restart unless-stopped \
    -d \
    linuxserver/sonarr:latest
