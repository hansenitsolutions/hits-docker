#!/bin/bash
docker image pull linuxserver/transmission:latest
docker stop transmission
docker rm transmission
docker run \
    --name transmission \
    -p 9091:9091 \
    -p 51413:51413 \
    -p 51413:51413/udp \
    -e PUID=1 -e PGID=1 \
    -v /etc/localtime:/etc/localtime:ro \
    -v /opt/transmission:/config \
    -v /mnt/downloads:/downloads \
    --restart unless-stopped \
    -d \
    linuxserver/transmission:latest
