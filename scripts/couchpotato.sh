#!/bin/bash
docker stop couchpotato
docker rm couchpotato
docker image pull linuxserver/couchpotato:latest

docker run \
    --name couchpotato \
    -p 5050:5050 \ß
    -e PUID=1 -e PGID=1 \
    -e TZ=Austrlaia/Melbourne \
    -v /etc/localtime:/etc/localtime:ro \
    -v /opt/couchpotato:/config \
    -v /mnt/media/movies:/movies \
    -v /mnt/downloads:/downloads \
    -d \
    --restart unless-stopped \
    linuxserver/couchpotato:latest
