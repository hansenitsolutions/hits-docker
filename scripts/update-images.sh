#!/bin/bash

docker pull linuxserver/transmission:latest

docker pull linuxserver/radarr:latest

docker pull linuxserver/sonarr:latest

docker-compose down

docker system prune --all --force

docker-compose up -d --build
