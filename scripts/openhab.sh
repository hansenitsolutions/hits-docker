#!/bin/bash
docker image pull openhab/openhab:latest

docker stop openhab
docker rm openhab

docker run \
        --name openhab \
        --net=host \
        --tty \
        -v /etc/localtime:/etc/localtime:ro \
        -v /opt/openhab/openhab_addons:/openhab/addons \
        -v /opt/openhab/openhab_conf:/openhab/conf \
        -v /opt/openhab/openhab_userdata:/openhab/userdata \
        -e "EXTRA_JAVA_OPTS=-Duser.timezone=Australia/Melbourne" \
        -d \
        --restart=always \
        openhab/openhab:latest

#        -v /etc/timezone/:/etc/timezone/:ro
