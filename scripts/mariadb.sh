#!/bin/bash

docker run \
    -e ALLOW_EMPTY_PASSWORD=yes \
    -v /opt/mariadb:/bitnami/mariadb \
    bitnami/mariadb:latest
