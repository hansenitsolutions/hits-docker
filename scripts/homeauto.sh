#!/bin/bash
docker stop home-assistant
docker rm home-assistant
docker image pull homeassistant/home-assistant:latest

docker run -d --name="home-assistant" -v /opt/homeassistant.config:/config -v /etc/localtime:/etc/localtime:ro --net=host homeassistant/home-assistant:latest
